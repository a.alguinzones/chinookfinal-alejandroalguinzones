﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace ChinookFinal.Data.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly DbContext _dbContext;

        public Repository(DbContext context)
        {
            _dbContext = context;
        }

        public void Actualizar(T registro)
        {
            throw new NotImplementedException();
        }

        public void Eliminar(T registro)
        {
            _dbContext.Set<T>().Remove(registro);
        }

        public void Insertar(T registro)
        {
            _dbContext.Set<T>().Add(registro);
        }

        public T ObtenerPorId(int id)
        {
            return _dbContext.Set<T>().Find(id);
        }

        public List<T> ObtenerTodo()
        {
            return _dbContext.Set<T>().ToList();
        }
    }
}
