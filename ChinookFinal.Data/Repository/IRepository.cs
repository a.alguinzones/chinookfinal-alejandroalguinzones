﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChinookFinal.Data.Repository
{
   public interface IRepository<T>
    {
        T ObtenerPorId(int id);
        List<T> ObtenerTodo();
        void Insertar(T registro);
        void Eliminar(T registro);
        void Actualizar(T registro);
    }
}


