﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChinookFinal.Data.Repository;
using ChinookFinal.Model;

namespace ChinookFinal.Data
{
  public  interface IUnitOfWork : IDisposable
    {
        void Commit();

        //indicar los Irepositories que trabajaran con el Unit
        #region Repositories
        IRepository<Employee> REmployee { get; }
        #endregion
    }
}
