﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using ChinookFinal.Model;


namespace ChinookFinal.Data
{
    public class ExamContext:DbContext
    {
        public ExamContext() : base("server=.;database=Chinook;Trusted_Connection=true")
        {
            Database.SetInitializer<ExamContext>(null);
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // quitar la convención de pluralización de tablas
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<Employee> Employees { get; set; }
    }
}
