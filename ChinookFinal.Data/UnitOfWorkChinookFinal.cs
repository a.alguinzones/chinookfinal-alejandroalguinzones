﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChinookFinal.Model;
using ChinookFinal.Data.Repository;
using ChinookFinal.Data;
using System.Data.Entity;

namespace ChinookFinal.Data
{
    public class UnitOfWorkChinookFinal : IUnitOfWork
    {
        protected readonly DbContext _context;

        public UnitOfWorkChinookFinal() : base()
        {
            _context = new ExamContext();
            REmployee = new Repository<Employee>(_context);
        }

        public IRepository<Employee> REmployee { get; private set; }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}

