﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using ChinookFinal.Model;
using ChinookFinal.Data;
using ChinookFinal.Data.Repository;

namespace ChinookFinal.ChinookWeb.Pages.Employees
{
    public partial class List : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ListaEmpleados();
            }
        }

        private void ListaEmpleados( int PageIndex = 0)
        {
            GridEmpleados.PageIndex = PageIndex;
            var ListaEmpleado = new List<Employee>();
            try
            {
                using (var uow = new UnitOfWorkChinookFinal())
                {
                    ListaEmpleado = uow.REmployee.ObtenerTodo();
                }
                GridEmpleados.DataSource = ListaEmpleado;
                GridEmpleados.DataBind();
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void GridEmpleados_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ListaEmpleados(e.NewPageIndex);
        }

        protected void GridEmpleados_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "editar":
                    {

                        if (int.TryParse(e.CommandArgument.ToString(), out int Id) && Id > 0)
                        {
                            var emplado = new Employee();
                            UpdatePanelModal.Update();
                            using (var uow = new UnitOfWorkChinookFinal())
                            {
                                emplado = uow.REmployee.ObtenerPorId(Id);
                            }
                            txtNombre.Text = emplado.FirstName;
                            txtApellido.Text = emplado.LastName;
                            hdnIdEmpleado.Value = emplado.EmployeeId.ToString();
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "openModalKey", "openModal()", true);

                        }
                        break;
                    }
                case "eliminar":
                    {
                        if (int.TryParse(e.CommandArgument.ToString(), out int Id) && Id > 0)
                        {
                            using (var uow = new UnitOfWorkChinookFinal())
                            {
                                var deleteEmpleado = uow.REmployee.ObtenerPorId(Id);
                                uow.REmployee.Eliminar(deleteEmpleado);
                                uow.Commit();
                            }

                            
                        }

                        break;
                    }
                default: break;
            }
            ListaEmpleados();

        }

        protected void btnInsertar_Click(object sender, EventArgs e)
        {
            InsertarNuevoEmpleado();
        }

        private void InsertarNuevoEmpleado()
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "openModalKey", "openModal()", true);
            hdnIdEmpleado.Value = "0";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (int.TryParse(hdnIdEmpleado.Value, out int Id))
            {
                if (Id == 0)
                {
                    InsertarEmpleado();
                }
                else
                {
                    EditarEmpleado(Id);
                }

            }
        }

        private void EditarEmpleado(int id)
        {
            try
            {
                using (var uow = new UnitOfWorkChinookFinal())
                {

                    var old = uow.REmployee.ObtenerPorId(id);


                    old.FirstName = txtNombre.Text.Trim();
                    old.LastName = txtApellido.Text.Trim();

                    uow.Commit();

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "closeModalKey", "closeModal()", true);

                }
            
            }
            catch (Exception)
            {

                throw;
            }
            ListaEmpleados();
        }

        private void InsertarEmpleado()
        {
            var NewEmpleado = new Employee()
            {
                FirstName = txtNombre.Text,
                LastName = txtApellido.Text
            };
            try
            {
                using (var uow = new UnitOfWorkChinookFinal())
                {
                    uow.REmployee.Insertar(NewEmpleado);
                    uow.Commit();
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "closeModal", "closeModal()", true);
            }
            catch (Exception)
            {

                throw;
            }
            ListaEmpleados();
        }

        protected void BtnError_Click(object sender, EventArgs e)
        {
            throw new Exception("Este es un error de prueba");
        }
    }
}