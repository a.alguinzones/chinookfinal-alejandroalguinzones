﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout/Main.Master" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="ChinookFinal.ChinookWeb.Pages.Employees.List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTitulo" runat="server">
    Lista de empleados
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
         <div>
             <asp:Button ID="btnInsertar" runat="server" Text="NuevoEmpleado" OnClick="btnInsertar_Click" CssClass="btn btn-success" />
        </div>
            <div>
                <asp:Button ID="BtnError" runat="server" Text="Error!!!!!!" OnClick="BtnError_Click" CssClass="btn btn-info" />
            </div>
            <asp:GridView ID="GridEmpleados" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="5" OnPageIndexChanging="GridEmpleados_PageIndexChanging" OnRowCommand="GridEmpleados_RowCommand" CssClass="table table-bordered" Width="50%">
                <Columns>
                    <asp:BoundField DataField="EmployeeId" HeaderText="Código" />
                        <asp:BoundField DataField="FirstName" HeaderText="Nombre" />
                        <asp:BoundField DataField="LastName" HeaderText="Apellido" />
                        <asp:TemplateField HeaderText="Acciones">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CommandName="editar" CommandArgument='<%# Eval("EmployeeId") %>' CssClass="btn btn-link">Editar</asp:LinkButton>
                            <asp:LinkButton runat="server" CommandName="eliminar" CommandArgument='<%# Eval("EmployeeId") %>' CssClass="btn btn-link">Eliminar</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:HiddenField ID="hdnTipoAccion" runat="server" />
            <asp:HiddenField ID="hdnIdEmpleado" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
<%--INI MODAL--%>

            <div>
        <div id="Modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" runat="server" id="test">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <asp:Literal ID="LitEmpresa" runat="server">Empleados</asp:Literal></h5>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-6">
                                <label for="txtApellido">Apellido</label>
                                <asp:TextBox ClientIDMode="Static" runat="server" ID="txtApellido" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="form-group col-6">
                                <label for="txtNombre">Nombre</label>
                                <asp:TextBox ClientIDMode="Static" runat="server" ID="txtNombre" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <asp:UpdatePanel ID="UpdatePanelModal" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                 <asp:Button runat="server" ID="btnSave" CssClass="btn btn-primary" Text="guardar" OnClick="btnSave_Click" ></asp:Button>
                                                                                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>


<%--FIN MODAL--%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="cphScripts" runat="server">
    <script type="text/javascript">
        //abrir Modal
        function openModal() {
            $("#Modal").modal();
        }

        //cerrar Modal 
        function closeModal() {
            $("#Modal").modal('hide');
        }
    </script>
</asp:Content>
