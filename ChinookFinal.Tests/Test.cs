﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using ChinookFinal.Model;
using ChinookFinal.Data;
using ChinookFinal.Data.Repository;


namespace ChinookFinal.Tests
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void listarAll()
        {
            var Lista = new List<Employee>();
            using (var uow= new UnitOfWorkChinookFinal())
            {
                Lista = uow.REmployee.ObtenerTodo();
            }
            foreach (var item in Lista)
            {
                Console.WriteLine(item.EmployeeId);
            }
            Assert.IsNotNull(Lista);
        }
    }
}
